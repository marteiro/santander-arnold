import {StAction} from '/components/StAction.js';
export class StText extends HTMLElement {

    static get observedAttributes() { return []; }

    constructor(data) {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = `
            <style>
                :host{
                    display:block;
                    box-sizing: border-box;
                    border-top: var(--line-width) solid var(--color-white);
                    padding-top: var(--spacing);
                    margin-bottom: var(--spacing);
                    font-family: "grotesk_regular";
                    position:relative;
                }

                ::slotted(span), ::slotted(div){
                    font-size:var(--text-size-body);
                    color:var(--color-white);
                    display:block;
                }

                ::slotted(div){
                    margin-bottom: var(--spacing);
                }

                ::slotted(span) span[mark]{
                    color:var(--color-red);
                }

                :host .preTitle{
                    position:absolute;
                    top: var(--spacing-smaller);
                    font-size:var(--text-size-label);
                    color:var(--color-white);
                }

                :host audio{
                    display:none;
                }
                
            </style>
            <span class="preTitle">${(data && data.pretitle) ? data.pretitle : ''}</span>
            <slot></slot>
        `;

        // ${ data && data.audio ? '<audio src=".'+data.audio+'" type="audio/mpeg"></audio>' : ''}

        if(data){
            this.data=data;
            this._shadow = shadowRoot;
            if(typeof data.content=="string"){
                let span=document.createElement("span");
                span.innerHTML=data.content;
                this.appendChild(span);
            }else if(typeof data.content=="object"){
                
                data.content.forEach(textContent => {
                    if(typeof textContent=="string"){
                        let span=document.createElement("div");
                        span.innerHTML=textContent;
                        this.appendChild(span);
                    }else if(textContent.type && textContent.type=="StAction"){
                        let action=new StAction(textContent);
                        this.appendChild(action);
                    }
                });
            }

            
        }

        
    }

    connectedCallback() {

        // if(this.data && this.data.audio){
        //     this.audio=new window.VolumeSample(this.data.audio);
        //     window.subscribeAudio(this.audio);
        // }
    }
}

window.customElements.define('st-text', StText);
