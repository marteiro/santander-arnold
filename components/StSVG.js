export class StSVG extends HTMLElement {

    static get observedAttributes() { return [src]; }

    constructor(url) {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = `
            <style>
                :host{
                    display:block;
                    position:relative;
                }

                ::slotted(*) {
                    
                    display:block;
                    
                }

                :host .around{
                    padding: .3m;
                    font-size:var(--text-size-label);
                    background-color:var(--color-white);
                    display: inline-block;
                }

                svg{
                    --svg-height:var(--text-size-title);
                    --prop: calc(var(--original-height) / var(--svg-height));

                    height: var(--svg-height);
                    width: calc(var(--original-width) * var(--prop));
                    --stroke-width: calc(var(--line-width) * 3);
                    overflow:visible;
                    position:relative;
                }

                .container::before{
                    content:"";
                    width:100%;
                    height:100%;
                    display:grid;
                    position:absolute;
                    top:0px;
                    left:0px;
                    justify-items: center;
                    align-items:center;
                }
            </style>
            <div class="container"></div>
        `;
        this._shadow = shadowRoot;
        
        
          
        var oReq = new XMLHttpRequest();
        oReq.onload = this.loaded.bind(this);
        oReq.open("get", url || this.getAttribute("src"), true);
        oReq.send();
    }

    connectedCallback() {
    }

    loaded(event){
        let container=this._shadow.querySelector(".container");

        let observerOptions = {
            childList: true,
            attributes: false,
            subtree: false
        }

        let observer = new MutationObserver(()=>{
                this.originalWidth=parseFloat(container.firstElementChild.getAttribute("width"));
                this.originalHeight=parseFloat(container.firstElementChild.getAttribute("height"));
                container.style.setProperty("--original-width",this.originalWidth+"px");
                container.style.setProperty("--original-height",this.originalHeight+"px");
        });

        observer.observe(container, observerOptions);

        container.innerHTML=event.target.responseText;        
    }
}

window.customElements.define('st-svg', StSVG);
