<?php
require  __DIR__ . "/app/Share.php";
require  __DIR__ . "/app/Router.php";
require  __DIR__ . "/app/Meta.php";

$share = new Share;
$routes = !empty($share->shares) ? array_keys($share->shares) : null;
$router = new Router($routes);
$meta   = new Meta($share, $router);
