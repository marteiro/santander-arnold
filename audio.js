// Start off by initializing a new context.
context = new(window.AudioContext || window.webkitAudioContext)();

if (!context.createGain)
    context.createGain = context.createGainNode;
if (!context.createDelay)
    context.createDelay = context.createDelayNode;
if (!context.createScriptProcessor)
    context.createScriptProcessor = context.createJavaScriptNode;




function loadSounds(obj, soundMap, callback) {
    // Array-ify
    var names = [];
    var paths = [];
    for (var name in soundMap) {
        var path = soundMap[name];
        names.push(name);
        paths.push(path);
    }

    console.log(soundMap)
    bufferLoader = new BufferLoader(context, paths, function (bufferList) {
      
        for (var i = 0; i < bufferList.length; i++) {
            var buffer = bufferList[i];
            var name = names[i];
            obj[name] = buffer;
        }
        if (callback) {
            callback();
        }
    });
    bufferLoader.load();
}




function BufferLoader(context, urlList, callback) {
    this.context = context;
    this.urlList = urlList;
    this.onload = callback;
    this.bufferList = new Array();
    this.loadCount = 0;
}

BufferLoader.prototype.loadBuffer = function (url, index) {
    // Load buffer asynchronously
    var request = new XMLHttpRequest();
    request.open("GET", url, true);
    request.responseType = "arraybuffer";

    var loader = this;

    request.onload = function () {
        // Asynchronously decode the audio file data in request.response
        loader.context.decodeAudioData(
            request.response,
            function (buffer) {
                if (!buffer) {
                    console.error('error decoding file data: ' + url);
                    return;
                }
                loader.bufferList[index] = buffer;
                if (++loader.loadCount == loader.urlList.length)
                    loader.onload(loader.bufferList);
            },
            function (error) {
                console.error('decodeAudioData error', error);
            }
        );
    }

    request.onerror = function () {
        console.error('BufferLoader: XHR error');
    }

    request.send();
};

BufferLoader.prototype.load = function () {
    for (var i = 0; i < this.urlList.length; ++i)
        this.loadBuffer(this.urlList[i], i);
};


/*
* Copyright 2013 Boris Smus. All Rights Reserved.

* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


function VolumeSample(files, callback) {
    
    loadSounds(this, files, callback);
 
    // function onLoaded() {
    //     this.play()
    //     console.log(1)
    //     // var button = document.querySelector('button');
    //     // button.removeAttribute('disabled');
    //     // button.innerHTML = 'Play/pause';
    // };
    this.isPlaying = false;
};

VolumeSample.prototype.play = function (audio) {
    this.isPlaying=true;
    this.gainNode = context.createGain();
    this.source = context.createBufferSource();
    
    this.source.buffer = this[audio];

    // Connect source to a gain node
    this.source.connect(this.gainNode);
    // Connect gain node to destination
    this.gainNode.connect(context.destination);
    // Start playback in a loop
    this.source.loop = false;
    this.source[this.source.start ? 'start' : 'noteOn'](0);
};

VolumeSample.prototype.changeVolume = function (element) {
    var volume = element.value;
    var fraction = parseInt(element.value) / parseInt(element.max);
    // Let's use an x*x curve (x-squared) since simple linear (x) does not
    // sound as good.
    this.gainNode.gain.value = fraction * fraction;
};

VolumeSample.prototype.stop = function () {
    this.isPlaying=false;
    this.source[this.source.stop ? 'stop' : 'noteOff'](0);
};