<?php

class Share
{
    public $data;
    public $shares;

    public function __construct()
    {
        $file = "./data.js";

        if (file_exists($file)) {
            $this->setData(file_get_contents($file));
        }

        if (!is_null($this->data)) {
            $this->setShares($this->data);
        }

    }

    public function setData($content)
    {
        $content = $this->remeveExportDefault($content);
        $json = json_decode($content, true);
        if (!is_null($json)) {
            $this->data = $json;
        }
    }

    private function remeveExportDefault($content)
    {
        return str_replace('export default ', '', $content);
    }

    public function setShares($data)
    {
        $shares = [];
        if (is_array($data) || is_object($data) && !empty($data['pages'])) {
            foreach ($data['pages'] as $contents) {
                if (!empty($contents['sharemeta'])) {
                    $uri = $contents['sharemeta']['uri'];

                    $shares[$uri]['title'] = !empty($contents['sharemeta']['title']) ? $contents['sharemeta']['title'] : null;
                    $shares[$uri]['img'] = !empty($contents['sharemeta']['img']) ? $contents['sharemeta']['img'] : null;
                    $shares[$uri]['description'] = !empty($contents['sharemeta']['description']) ? $contents['sharemeta']['description'] : null;

                    if ($this->hasGrandsonToShare($contents)) {
                        $grandsonShare = $this->setGrandsonShare($contents['content']);
                        foreach ($grandsonShare as $grandsonUri => $grandsonContent) {
                            $shares[$uri . $grandsonUri]['title'] = !empty($grandsonContent['title']) ? $grandsonContent['title'] : null;;
                            $shares[$uri . $grandsonUri]['img'] = !empty($grandsonContent['img']) ? $grandsonContent['img'] : null;;
                            $shares[$uri . $grandsonUri]['description'] = !empty($grandsonContent['description']) ? $grandsonContent['description'] : null;;
                        }
                    }

                }
            }
        }

        $this->shares = $shares;
    }

    private function hasGrandsonToShare($contents)
    {
        if (!empty($contents['content']) && is_array($contents['content']) || is_object($contents['content'])) {
            foreach ($contents['content'] as $grandsonContent) {

                if ($grandsonContent['type'] == "StSliderStaticTitle" && !is_null($grandsonContent['share'])) {
                    return true;
                }

                return false;
            }
        }
    }

    private function setGrandsonShare($contents)
    {
        $shares = [];
        $contents = !empty($contents[0]) ? $contents[0] : null;

        foreach ($contents['content'] as $content) {
            if (!empty($content['sharemeta'])) {
                $uri = $content['sharemeta']['uri'];

                $shares[$uri]['title'] = !empty($content['sharemeta']['title']) ? $content['sharemeta']['title'] : null;
                $shares[$uri]['img'] = !empty($content['sharemeta']['img']) ? $content['sharemeta']['img'] : null;
                $shares[$uri]['description'] = !empty($content['sharemeta']['description']) ? $content['sharemeta']['description'] : null;

                if ($this->hasGrandsonToShare($content)) {
                    $this->setGrandsonShare($content['content']);
                }

            }
        }

        return $shares;
    }
}
