import {StSVG} from '/components/StSVG.js';
export class StToggleIcon extends HTMLElement {

    static get observedAttributes() { return []; }

    constructor() {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = `
            <style>
                :host{
                    display:block;
                }

                st-svg{
                    display:block;
                    grid-column:1;
                    grid-row:1;
                    opacity:0;
                    transition: all .2s linear;
                    justify-self:end;
                }

                st-svg[enabled]{
                    opacity:1;
                }

                #icon1{
                    --stroke-color: var(--color-one);
                }

                #icon2{
                    --stroke-color: var(--color-two, var(--color-one));
                }

                #container{
                    display:block;
                    height:var(--spacing);
                    display:grid;
                    grid-template-rows:1fr;
                    grid-template-columns:1fr;
                }
            </style>
            <div id="container">
                ${this.hasAttribute("icon1") ? "<st-svg id='icon1' src='"+this.getAttribute("icon1")+ (this.getAttribute("enabled")==1 || (this.getAttribute("enabled")!=2 && !this.hasAttribute('enabled')) ? "' enabled"  : "'") +"></st-svg>" : ''}
                ${this.hasAttribute("icon2") ? "<st-svg id='icon2' src='"+this.getAttribute("icon2")+ (this.getAttribute("enabled")==2 ? "' enabled" : "'") +"></st-svg>" : ''}
            </div>
        `;
        this._shadow = shadowRoot;

        this.icons=this._shadow.querySelectorAll("st-svg");
        
        this.selected=this.icons[0].hasAttribute('enabled') ? 0 : 1
        if(this.selected==0){
            this.setAttribute("one", '');
        }else{
            this.setAttribute("two", '');
        }

        this.addEventListener("click", this.toggle.bind(this))
    }

    connectedCallback() {
        
    }

    toggle(){
        this.icons[this.selected].removeAttribute('enabled')

        if(this.selected==0){
            this.selected=1;            
        }else{
            this.selected=0;
        }

        this.toggleAttribute("two");
        this.toggleAttribute("one");

        this.icons[this.selected].setAttribute('enabled', '');
    }

    setIcon(n){
        if(n==1){
            this.icons[0].removeAttribute("selected")
            this.icons[1].setAttribute("selected", '')
        }else{
            this.icons[1].removeAttribute("selected")
            this.icons[0].setAttribute("selected", '')
        }
    }

}

window.customElements.define('st-toggle-icon', StToggleIcon);
