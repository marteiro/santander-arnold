<?php

class Router
{
    public $routes;
    public $uri;
    public $currentDomain;

    public function __construct($routes = null)
    {
        $this->setCurrentDomain();
        $this->setUri();

        if (!is_null($routes)) {
            $this->setRoutes($routes);
        }

        if (!$this->isValidRoute()) {
            // http_response_code(404);
            // header("Location: /404");
        }
    }

    public function setRoutes($routes)
    {
        $r = [];

        if (is_array($routes) || is_object($routes)) {
            foreach ($routes as $route) {
                $r[] = $this->sanitize($route);
            }
        }

        $this->routes = $r;
    }

    public function setUri($uri = null)
    {
        if (!is_null($uri)) {
            $this->uri = $this->sanitize($uri);
        }

        $this->uri = $this->sanitize($_SERVER['REQUEST_URI']);;
    }

    public function setCurrentDomain()
    {
        $current = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
        $this->currentDomain = $this->sanitize($current);
    }

    private function sanitize($str)
    {
        if ($str == '/') {
            return $str;
        }

        return rtrim($str, '/');
    }

    private function isValidRoute($route = null)
    {
        if (!is_null($route)) {
            return in_array($route, $this->routes);
        }

        $uri = preg_replace('/\?.*+/m', '', $this->uri); // remove query string for match routes
        return in_array($uri, $this->routes);
    }
}
