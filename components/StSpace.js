export class StSpace extends HTMLElement {

    static get observedAttributes() { return []; }

    constructor() {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = `
            <style>
                :host{
                    display:block;
                    height:var(--spacing);
                }
            </style>
        `;
        this._shadow = shadowRoot;
    }

    connectedCallback() {
    }
}

window.customElements.define('st-space', StSpace);
