import {StAction} from '/components/StAction.js';
import {StLabel} from '/components/StLabel.js';
import {StSliderStaticTitle} from '/components/StSliderStaticTitle.js';
import {StTitle} from '/components/StTitle.js';
import {StText} from '/components/StText.js';
import {StSpace} from '/components/StSpace.js';
import {StSVG} from '/components/StSVG.js';

export class StPage extends HTMLElement {

    static get observedAttributes() { return []; }

    constructor(data) {
        let classes={
            'StAction':StAction,
            'StLabel':StLabel,
            'StSliderStaticTitle':StSliderStaticTitle,
            'StTitle':StTitle,
            'StText':StText,
            'StSpace':StSpace,
            'StSVG':StSVG
        }

        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = `
            <style>
                :host{
                   position:relative;
                   width:100vw;
                   height:100%;
                   display:flex;
                   flex-direction: column;
                   justify-content: center;
                   scroll-snap-align: start start;
                   -webkit-scroll-snap-align: start start;
                   padding: 0 var(--spacing);
                   box-sizing:border-box;
                   justify-self: center;
                   max-width:1000px;
                }
            </style>
            <slot></slot>
            
        `;
        this._shadow = shadowRoot;
        this.data=data;
        this.cue=this.data.cue || undefined;

        data.content.forEach(content => {
            // console.log(content.type, content.content)
             let contentItem=new classes[content.type](content ? content : null);

             if(content.call){
                contentItem.addEventListener("click", window[content.call]);
             }
             this.appendChild(contentItem);
        });
        
    }

    connectedCallback() {
        if(this.last){
            setTimeout(()=>{
                window.menuClick(null,this.last)
            },50)
        }
    }

    select(){
        if(this.data.cue){
            window.changeCue(this.data.cue);
        }
        if(this.data.sharemeta){
            if(!this.slider){
                history.pushState({},this.data.title,this.data.sharemeta.uri);
            }else{
                history.pushState({},this.data.title,this.data.sharemeta.uri+this.slider.items[this.slider.selected].data.sharemeta.uri);
            }
        }
        if(this.slider && this.slider.items[this.slider.selected].data.audio){
            window.playAudio(this.slider.items[this.slider.selected].data.audio);
        }
    }

    deselect(){
        if(this.slider && this.slider.items[this.slider.selected].data.audio){
            window.leaveAudio();
        }
    }

    setSub(slider){
        this.slider=slider;
    }
}

window.customElements.define('st-page', StPage);
