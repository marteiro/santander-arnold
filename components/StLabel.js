export class StLabel extends HTMLElement {

    static get observedAttributes() { return []; }

    constructor(data) {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = `
            <style>
                :host{
                    display:block;
                    position:relative;
                    margin-bottom:var(--spacing);
                    font-family: "grotesk_demi";
                    cursor:pointer;
                }

                ::slotted(*) {
                    line-height:var(--text-size-title);
                    display:block;
                    
                }

                :host .around{
                    padding: 0 .3em;
                    font-size:var(--text-size-label);
                    background-color:var(--color-white);
                    display: inline-block;
                }
            </style>
            <div class="around">
                <slot></slot>
            </div>
        `;
        this._shadow = shadowRoot;
        if(data){
            let span=document.createElement("span");
            span.innerHTML=data.content;
            this.appendChild(span);
        }
    }

    connectedCallback() {
    }
}

window.customElements.define('st-label', StLabel);
