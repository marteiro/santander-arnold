import { StSVG } from '/components/StSVG.js';
export class StAction extends HTMLElement {

    static get observedAttributes() { return []; }

    constructor(data) {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = `
            <style>
                :host{
                    display:grid;
                    box-sizing:border-box;
                    padding-top: var(--spacing-smaller);
                    border-top: var(--line-width) solid var(--color-white);
                    margin-bottom: var(--spacing);
                    cursor:pointer;
                    background-color:transparent;
                    grid-template-columns: 1fr min-content auto;
                    position:relative;
                }

                :host span{
                    font-size: var(--text-size-label);
                    margin:0;
                    display:block;
                    color:var(--color-white);
                }

                @media screen and (min-width: 800px) {
                    :host span[bigger]{
                        font-size: var(--text-size-body);
                    }
                }

                :host .others{
                    display:grid;
                    grid-template-columns: repeat(3, min-content);
                    grid-gap:var(--spacing);
                    margin-right:var(--spacing);
                    --fill-color:var(--color-white);
                    --svg-height:4pt;
                    z-index:4;
                    visibility:hidden;
                    transition: visibility .1s;
                    transition-delay: .6s;
                }

                :host .others[active]{
                    visibility:visible;
                    transition-delay: .01s;
                }

                :host .others a{
                    transform: scale3d(0,0,0);
                    transition: transform .3s ease-in-out;
                }

                :host .others[active] a{
                    transform: scale3d(1,1,1)
                }

                :host .others>a:nth-child(1){
                    transition-delay:.3s;
                }
                :host .others>a:nth-child(2){
                    transition-delay:.2s;
                }
                :host .others>a:nth-child(3){
                    transition-delay:.1s;
                }

                st-svg{
                    --stroke-color:var(--color-white, red);
                    cursor:pointer;
                }
                
                img{
                    height: calc(1.5 * var(--text-size-title));
                }

                :host .bg{
                    position:absolute;
                    width:100%;
                    height:100%;
                    background-color:transparent;
                    z-index:2;
                }
            </style>
            <div class="others"></div>
            <div class="bg"></div>
        `;
        this.data=data;
        this._shadow = shadowRoot;
        this.others=this._shadow.querySelector(".others");
        this.bg=this._shadow.querySelector(".bg");
        let span=document.createElement("span");
        span.innerHTML=data.title;

        
        if(this.data.params){
            span.setAttribute(this.data.params, '');
        }
        this._shadow.insertBefore(span, this.others);

        if(data.icon=='share'){
            let facebook= new StSVG("/assets/facebook.svg");
            let twitter= new StSVG("/assets/twitter.svg");
            let linkedin= new StSVG("/assets/linkedin.svg");

            let facebookA=document.createElement("a");
            facebookA.target="_blank"
            facebookA.id="facebook"
            facebookA.href="https://www.facebook.com/sharer/sharer.php?u="+data.data;
            let twitterA=document.createElement("a");
            twitterA.target="_blank"
            twitterA.id="twitter"
            twitterA.href="https://twitter.com/intent/tweet?text="+data.data;
            let linkedinA=document.createElement("a");
            linkedinA.id="linkedin"
            linkedinA.target="_blank"
            linkedinA.href="https://www.linkedin.com/shareArticle?mini=true&url="+data.data;

            facebookA.addEventListener("click", this.gaEvent.bind(this, facebookA, "facebook"));
            twitterA.addEventListener("click", this.gaEvent.bind(this, twitterA, "twitter"));
            linkedinA.addEventListener("click", this.gaEvent.bind(this, linkedinA, "linkedin"));

            this.facebookA=facebookA;
            this.twitterA=twitterA;
            this.linkedinA=linkedinA;

            facebookA.appendChild(facebook);
            twitterA.appendChild(twitter);
            linkedinA.appendChild(linkedin);

            this.others.appendChild(facebookA);
            this.others.appendChild(twitterA);
            this.others.appendChild(linkedinA);
        }
        
        let svg=new StSVG("/assets/"+data.icon + ".svg");
        let svgA=document.createElement("a");
        svgA.appendChild(svg);
        this._shadow.insertBefore(svgA, this.bg);

        if(data.icon=='share'){
            this._shadow.querySelector(".bg").addEventListener("click", this.activateShare.bind(this));
        }else if(typeof data.data){
            this._shadow.querySelector(".bg").addEventListener("click", this.action.bind(this, event, data.data));          
        }
        
        this.timeout;
    }

    gaEvent(tgt, label, e){
        console.log(arguments);
        // Event Category: socialshare
        // Event Action: click
        // Event Label: facebook, twitter or linkedin (depending on which icon they click).
        // ga(‘send’, ‘event’, ‘Category’, ‘Action’, ‘Label’, ‘Value’);
        ga('send', 'event', 'socialshare', 'click',  label);
        // gtag('event', 'socialshare', {
        //     'action': 'click',
        //     'label': label,
        //     'value': tgt.href
        //   });
    }

    changeTarget(){
        this.facebookA.href="https://www.facebook.com/sharer/sharer.php?u="+this.data.data;
        this.twitterA.href="https://twitter.com/intent/tweet?text="+this.data.data;
        this.linkedinA.href="https://www.linkedin.com/shareArticle?mini=true&url="+this.data.data;
    }

    action(evt, data){
        if(data.type && data.type=="globalFunction"){
            window[data.name](...data.params);
        }else if(data.type && data.type=="link"){
            window.open(data.url, "_blank");
        }
    }

    activateShare(){
        clearTimeout(this.timeout);
        this.others.setAttribute("active", '');
        this.timeout=setTimeout(this.deactivateShare.bind(this),3000);
    }

    deactivateShare(){
        this.others.removeAttribute("active")
    }

    connectedCallback() {

    }
}

window.customElements.define('st-action', StAction);
