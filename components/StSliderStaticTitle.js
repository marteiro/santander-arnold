import {StTitle} from '/components/StTitle.js';
import { StText } from '/components/StText.js';
import { StAction } from '/components/StAction.js';
export class StSliderStaticTitle extends HTMLElement {

    static get observedAttributes() { return []; }

    constructor(data) {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = `
            <style>
                :host{
                    display: grid;
                    grid-template-columns: 30% 1fr;
                    grid-template-rows:auto auto;
                    grid-column-gap: var(--spacing);
                    column-gap: var(--spacing);
                    position:relative;
                }

                :host .crop{
                    display:grid;
                    grid-gap:var(--spacing);
                    overflow-x:auto;
                    grid-template-columns: repeat(${data.content.length},100%);
                    overflow-y: auto;
                    scroll-snap-type: x mandatory;
                    -webkit-scroll-snap-type: x mandatory;
                    -webkit-overflow-scrolling: touch;
                    
                }

                :host .crop::-webkit-scrollbar {
                    display: none;
                }

                :host .crop::-webkit-scrollbar-thumb {
                    background: #F00; 
                }

                ::slotted(st-text){
                    scroll-snap-align: start start;
                    -webkit-scroll-snap-align: start start;
                    font-family: "grotesk_demi";
                }

                :host .dots{
                    /*--spacing-smaller: 5pt;
                    --line-width: 2px;*/

                    position:absolute;
                    display:inline-block;
                    top: calc(var(--spacing-smaller) + (var(--text-size-label) / 2));
                    right: var(--line-width);
                    display:grid;
                    grid-template-columns: repeat(${data.content.length}, 1fr);
                    grid-gap: calc(var(--spacing-smaller) + (var(--text-size-label) / 2));
                    cursor: pointer;
                }

                @media screen and (min-width: 800px) {
                    /*:host .dots{
                        --spacing-smaller: 8pt;
                        --line-width: 2px;
                    }*/
                }

                :host .dots > div{
                    width:var(--spacing-smaller);
                    height:var(--spacing-smaller);
                    background-color:var(--color-white);
                    position:relative;
                    border-radius:100%;
                }

                :host .dots > div:after{
                    content:"";
                    width:calc(var(--spacing-smaller) * 2);
                    height:calc(var(--spacing-smaller) * 2);
                    border: var(--line-width) solid var(--color-white);
                    display:block;
                    border-radius: 100%;
                    transform:translate3d(calc(-50% + (var(--spacing-smaller) / 2)), calc(-50% + (var(--spacing-smaller) / 2)), 0) scale(0,0);
                    position:absolute;
                    top:0;
                    left:0;
                    transition: transform .3s ease-in-out;
                }

                :host .dots > div:before{
                    content:"";
                    width:calc(var(--spacing-smaller) * 2);
                    height:calc(var(--spacing-smaller) * 2);
                    top:0;
                    left:0;
                    display:block;
                    position:absolute;
                    
                    transform: translate3d(-50%, -50%, 0);
                }

                
                :host .dots > div[selected]:after{
                    transform: translate3d(calc(-50% + (var(--spacing-smaller) / 2)), calc(-50% + (var(--spacing-smaller) / 2)), 0) scale(1,1);
                }

                :host st-action{
                    grid-column: 1 / span 2;
                }
                
            </style>
            <slot name="title"></slot>
            <div class="crop">
                <slot></slot>
            </div>
            <div class="dots"></div>
        `;
        this._shadow = shadowRoot;

        this.crop=this._shadow.querySelector(".crop");
        
        this.data=data;
        let title=new StTitle(data.title);
        title.setAttribute("slot", "title");
        this.appendChild(title);

        this.dots=[]
        this.items=[]
        this.selected=0;

        if(data.content.length>0){

            data.content.forEach((content, i) => {
                let text=new StText(content);
                this.items.push(text);
                this.appendChild(text);

                if(data.content.length>1){
                    let dot=document.createElement('div');
                    dot.addEventListener("click", this.scrollTo.bind(this, i))
                    this.dots.push(dot);
                    this._shadow.querySelector(".dots").appendChild(dot);
                }
            });
        }

        if(data.content.length>1){
            this.crop.addEventListener("scroll", this.scrollCheck.bind(this));
            this.dots[0].setAttribute("selected", "");
        }


        if(data.share && data.share.type=="StAction"){
            this.share=new StAction(data.share);
            this._shadow.appendChild(this.share);
        }
    }

    scrollTo(i){
        let d=this.items[1].offsetLeft-this.items[0].offsetLeft;
        this.crop.scrollLeft = d*i;
    }

    scrollCheck(e){
        let w=e.srcElement.scrollWidth;
        let l=e.srcElement.scrollLeft;
        let p=this.items[1].offsetLeft-this.items[0].offsetLeft;
        let wt=w-p;

        let newSelected=Math.round(l/p);

        if(newSelected!=this.selected){
            this.dots[this.selected].removeAttribute("selected");
            this.selected=newSelected;
            this.dots[this.selected].setAttribute("selected", "");

            this.setParent();
            
            if(this.items[this.selected].data.cue){
                window.changeCue(this.items[this.selected].data.cue);
            }

            if(this.items[0].data.sharemeta){
                if(this.share){
                    this.share.data.data=location.origin+"/"+location.pathname.split('/')[1]+this.items[this.selected].data.sharemeta.uri;
                    this.share.changeTarget();
                }
                this.parentElement.select()
            }
            if(this.items[this.selected].audio){
                window.playAudio(this.items[this.selected].audio);
            }
        }
    }

    connectedCallback() {
        if(this.items[0].data.sharemeta){
            this.parentElement.setSub(this);
            if(this.share){
                this.share.data.data=location.origin+"/"+location.pathname.split('/')[1]+this.items[this.selected].data.sharemeta.uri;
                this.share.changeTarget();
            }
        }


        if(this.share){
            this.share.changeTarget();
        }

        this.setParent();
    }

    setParent(){
        if(this.items[0].data.cue){
            this.parentElement.data.cue=this.items[this.selected].data.cue;
        }
    }
}

window.customElements.define('st-slider-static-title', StSliderStaticTitle);
