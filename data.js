export default {
    "pages":[
        {
            "title":"Home",
            "sharemeta":{
                "uri":"/",
                "img":"/assets/tagimg.jpg",
                "title":"The most dangerous place for women is the home.",
                "description":"Santander is partnering with NNEDV and NCADV to help domestic violence survivors regain their financial independence and escape abuse. Watch the film and learn more at InSomeoneElsesShoes.com."
            },
            "cue":{
                "start":0.5,
                "end":24
            },
            "content":[
                {
                    "type":"StLabel",
                    "content": "IN SOMEONE ELSE'S SHOES <div class='arrow'>&#9654&#xFE0E</div>",
                    "antion":"call overlay",
                    "call":"showHotspot"
                },{
                    "type":"StTitle",
                    "pretitle" : null,
                    "content": "Escaping domestic violence is never as easy as “just leaving.”"
                },{
                    "type":"StAction",
                    "icon":"play",
                    "title": "PLAY VIDEO",
                    "data": {
                        "type":"globalFunction",
                        "name":"playVideo",
                        "params":[]
                    },
                    "params":"bigger"
                },{
                    "type":"StAction",
                    "icon":"share",
                    "title": "SHARE",
                    "data": "https://www.insomeoneelsesshoes.com",
                    "params":"bigger"
                }
            ]
        },{
            "title":"About Domestic Violence",
            "sharemeta":{
                "uri":"/about-domestic-violence",
                "img":"/assets/tagimg.jpg",
                "title":"The most dangerous place for women is the home.",
                "description":"Santander is partnering with NNEDV and NCADV to help domestic violence survivors regain their financial independence and escape abuse. Watch the film and learn more at InSomeoneElsesShoes.com."
            },
            "content":[
                {
                    "type": "StSliderStaticTitle",
                    "title": {
                        "type":"StTitle",
                        "pretitle": "ABOUT",
                        "content": "The truth about domestic violence"
                    },
                    "share":{
                        "type":"StAction",
                        "icon":"share",
                        "title": "SHARE",
                        "data": null,
                        "params":"bigger"
                    },
                    "content":[
                        {
                            "type":"StText",
                            "content":"<span mark>99%</span> of all domestic abuse cases include financial abuse.",
                            "sharemeta":{
                                "uri":"/fact1",
                                "img":"/assets/fact1.jpg",
                                "title":"The most dangerous place for women is the home.",
                                "description":"Santander is partnering with NNEDV and NCADV to help domestic violence survivors regain their financial independence and escape abuse. Watch the film and learn more at InSomeoneElsesShoes.com."
                            },
                            "audio":"fact1",
                            "cue":{
                                "start":26,
                                "end":46
                            }
                        },{
                            "type":"StText",
                            "content":"<span mark>1 in 4</span> women will experience domestic abuse in their lifetime.",
                            "sharemeta":{
                                "uri":"/fact2",
                                "img":"/assets/fact2.jpg",
                               "title":"The most dangerous place for women is the home.",
                                "description":"Santander is partnering with NNEDV and NCADV to help domestic violence survivors regain their financial independence and escape abuse. Watch the film and learn more at InSomeoneElsesShoes.com."
                            },
                            "audio":"fact2",
                            "cue":{
                                "start":48,
                                "end":65
                            }
                        },{
                            "type":"StText",
                            "content":"<span mark>Every 60 seconds</span>, 24 people are victims of rape, physical violence, or stalking by an intimate partner.",
                            "sharemeta":{
                                "uri":"/fact3",
                                "img":"/assets/fact3.jpg",
                               "title":"The most dangerous place for women is the home.",
                                "description":"Santander is partnering with NNEDV and NCADV to help domestic violence survivors regain their financial independence and escape abuse. Watch the film and learn more at InSomeoneElsesShoes.com."
                            },
                            "audio":"fact3",
                            "cue":{
                                "start":67,
                                "end":71
                            }
                        },{
                            "type":"StText",
                            "content":"Survivors return to their abusers <span mark>many times</span> before they leave for good.",
                            "sharemeta":{
                                "uri":"/fact4",
                                "img":"/assets/fact4.jpg",
                               "title":"The most dangerous place for women is the home.",
                                "description":"Santander is partnering with NNEDV and NCADV to help domestic violence survivors regain their financial independence and escape abuse. Watch the film and learn more at InSomeoneElsesShoes.com."
                            },
                            "audio":"fact4",
                            "cue":{
                                "start":73,
                                "end":98
                            }
                        }
                    ]
                }
            ]
        },{
            "title":"About financial abuse",
            "sharemeta":{
                "uri":"/about-financial-abuse",
                "img":"/assets/tagimg.jpg",
                "title":"The most dangerous place for women is the home.",
                "description":"Santander is partnering with NNEDV and NCADV to help domestic violence survivors regain their financial independence and escape abuse. Watch the film and learn more at InSomeoneElsesShoes.com."
            },
            "cue":{
                "start":67,
                "end":71
            },
            "content":[
                {
                    "type": "StSliderStaticTitle",
                    "title": {
                        "type":"StTitle",
                        "pretitle" : "ABOUT",
                        "content": "Domestic abuse often doesn't end at physical and emotional abuse."
                    },
                    "content":[
                        {
                            "type":"StText",
                            "content":"In fact, the least discussed form of abuse is one of the biggest barriers to leaving. Financial abuse is one of the main reasons why victims and survivors of domestic violence struggle to escape abusive partners.",
                            "shareURL":"http://www.apple.com"
                        }
                    ]
                },{
                    "type": "StSpace"
                },{
                    "type": "StSliderStaticTitle",
                    "title": {
                        "type":"StTitle",
                        "pretitle" : null,
                        "content": "What might financial abuse look like?"
                    },
                    "content":[
                        {
                            "type":"StText",
                            "content":"<h1>01.</h1> <span mark>Locking them out:</span> Abusers will limit their victims’ access to money and lock them out of their bank accounts. Victims have to face the choice of poverty or staying with an abusive partner.",
                            "shareURL":""
                        },{
                            "type":"StText",
                            "content":"<h1>02.</h1> <span mark>Sabotaging employment:</span> Abusive partners may harass their victims at work, prevent them from going to work, withhold transportation or childcare, or beat them severely to the point that they cannot work.",
                            "shareURL":""
                        },{
                            "type":"StText",
                            "content":"<h1>03.</h1> <span mark>Watching their every move:</span> Victims and survivors who do have access to the internet still may not be able to attain any online resources or information that could help them leave because many abusers monitor their victims’ internet use and activity.",
                            "shareURL":""
                        },{
                            "type":"StText",
                            "content":"<h1>04.</h1> <span mark>Destroying credit:</span> Some abusers trap victims through their social security number and credit score by overspending on the victim’s credit cards and refusing to pay bills.",
                            "shareURL":""
                        }
                    ]
                }
            ]
        },{
            "title":"What we are doing",
            "sharemeta":{
                "uri":"/what-we-are-doing",
                "img":"/assets/tagimg.jpg",
                "title":"The most dangerous place for women is the home.",
                "description":"Santander is partnering with NNEDV and NCADV to help domestic violence survivors regain their financial independence and escape abuse. Watch the film and learn more at InSomeoneElsesShoes.com."
            },
            "cue":{
                "start":100,
                "end":118
            },
            "content":[
                {
                    "type": "StSliderStaticTitle",
                    "title": {
                        "type":"StTitle",
                        "pretitle": "WHAT WE ARE DOING",
                        "content": "We can do something about this. We can help survivors reclaim their freedom. "
                    },
                    "share":null,
                    "content":[
                        {
                            "type":"StText",
                            "content":"Achieving financial independence gives victims and survivors a much better chance at successfully escaping their abusers. Santander is committed to helping survivors reclaim their identity and reach financial freedom. We are working with the NCADV to offer access to financial education for survivors of domestic violence, and we’re providing funding for the NNEDV,  micro-lending program.",
                            "shareURL":""
                        }
                    ]
                },{
                    "type":"StAction",
                    "icon":"plus",
                    "title": "SEE MORE",
                    "data": {
                        "type":"globalFunction",
                        "name":"showPink",
                        "params":[]
                    }
                },{
                    "type":"StSpace"
                },{
                    "type":"StText",
                    "pretitle":"FINANCIAL EDUCATION WEBINARS",
                    "content":"These webinars help domestic violence survivors understand their finances so they can reach financial independence. They provide useful information for how to make a budget, find housing, build credit, and achieve financial stability. Each webinar is free and provided in partnership with the NCADV. They are available through EverFi.",
                    "shareURL":"",
                    "action":"changeVideo",
                    "data":"videoPosition"
                },{
                    "type":"StAction",
                    "icon":"plus",
                    "title": "WATCH THE WEBINARS HERE",
                    "data": {
                        "type":"link",
                        "url":"https://ncadv.everfi-next.net"
                    }
                }
            ]
        },{
            "title":"How you can help",
            "sharemeta":{
                "uri":"/how-you-can-help",
                "img":"/assets/tagimg.jpg",
                "title":"The most dangerous place for women is the home.",
                "description":"Santander is partnering with NNEDV and NCADV to help domestic violence survivors regain their financial independence and escape abuse. Watch the film and learn more at InSomeoneElsesShoes.com."
            },
            "cue":{
                "start":120,
                "end":128
            },
            "content":[
                {
                    "type": "StSliderStaticTitle",
                    "title": {
                        "type":"StTitle",
                        "pretitle": "HOW YOU CAN HELP",
                        "content": "You can help survivors reach freedom too. "
                    },
                    "share":null,
                    "content":[
                        {
                            "type":"StText",
                            "content":[
                                "<span mark>Share:</span> Sharing this site could save someone’s life.",
                                {
                                    "type":"StAction",
                                    "icon":"share",
                                    "title": "SHARE",
                                    "data": "https://www.insomeoneelsesshoes.com",
                                    "params":"bigger"
                                },
                                "<span mark>Donate: </span>Your donation could be the difference between someone’s safety and suffering.<br><br> <span mark><a data-v-76eb1f4a='' href='https://nnedv.org/donate-now/' id='donatennedv' target='_blank'>Donate to the NNEDV's micro-lending program here. &#8227;</a></span><br><br> <span mark><a data-v-76eb1f4a=' href='https://ncadv.org/donate' target='_blank'  id='donatencadv'>Donate to the NCADV here. &#8227;</a></span><br><br>All donations will go directly to the NNEDV and NCADV, not Santander."
                            ]
                        }
                    ]
                },{
                    "type":"StSpace"
                },{
                    "type":"StText",
                    "content":"<div style='text-align:center; font-size:var(--text-size-label)'>©2019 Santander Bank, N.A. All rights reserved, Member FDIC</div>"
                }
            ]
        }
    ]
}