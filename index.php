<?php require "bootstrap.php"; ?>
<!DOCTYPE html>
<html lang="en" style="background-color:black;">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-M748VBL');</script>
    <!-- End Google Tag Manager -->    


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/fonts/fonts.css">
    <link rel="stylesheet" href="/style.css">

    <meta property="og:title" content="<?php echo $meta->title?>">
    <meta property="og:description" content="<?php echo $meta->description?>">
    <meta property="og:image" content="<?php echo $router->currentDomain . $meta->img?>">
    <meta property="og:url" content="<?php echo $router->currentDomain . $router->uri ?>">

    <meta property="og:site_name" content="In Someone Else’s Shoes">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="og:type" content="website">

    <link rel="preload" href="/components/StPage.js" as="script">
    <link rel="preload" href="/components/StSVG.js" as="script">
    <link rel="preload" href="/components/StText.js" as="script">
    <link rel="preload" href="/components/StToggleIcon.js" as="script">

    <!-- generics -->
    <link rel="icon" href="/assets/icons/favicon-32.png" sizes="32x32">
    <link rel="icon" href="/assets/icons/favicon-57.png" sizes="57x57">
    <link rel="icon" href="/assets/icons/favicon-76.png" sizes="76x76">
    <link rel="icon" href="/assets/icons/favicon-96.png" sizes="96x96">
    <link rel="icon" href="/assets/icons/favicon-128.png" sizes="128x128">
    <link rel="icon" href="/assets/icons/favicon-192.png" sizes="192x192">
    <link rel="icon" href="/assets/icons/favicon-228.png" sizes="228x228">
    <!-- Android -->
    <link rel="shortcut icon" sizes="196x196" href=“/assets/icons/favicon-196.png">
    <!-- iOS -->
    <link rel="apple-touch-icon" href="/assets/icons/favicon-120.png" sizes="120x120">
    <link rel="apple-touch-icon" href="path/to/favicon-152.png" sizes="152x152">
    <link rel="apple-touch-icon" href="path/to/favicon-180.png" sizes="180x180">
    <!-- Windows 8 IE 10-->
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/assets/icons/favicon-144.png">
    <!-- Windows 8.1 + IE11 and above -->
    <meta name="msapplication-config" content="/assets/icons/browserconfig.xml" />


    <script src="https://player.vimeo.com/api/player.js"></script>

    <title>In Someone Else’s Shoes</title>
    <link rel="canonical" href="https://insomeoneelsesshoes.com">

    <script src="/audio.js"></script>
   

    <script type='module'>
        import {StPage} from '/components/StPage.js';
        import {StText} from '/components/StText.js';
        import {StSVG} from '/components/StSVG.js';
        import {StToggleIcon} from '/components/StToggleIcon.js';
        import data from '/data.js';

        window.selected=0; 
        let primeiroLink=location.pathname.split("/")[1];
        var uniqueProducts = data.pages.filter( function( elem, i, array ) {
            if(elem.sharemeta.uri=="/"+primeiroLink){
                window.selected=i;
                return true;
            }
            return false;
        });

        let container=document.querySelector(".container");
        let menu=document.querySelector("nav");
        let menuButton=document.querySelector(".menuButton");
        window.pages=[];

        let toogleMenu=(evt, cmd)=>{
            if(cmd===false){
                menu.setAttribute('hidden', '');
            }else if(cmd===true){
                menu.removeAttribute('hidden');
            }else if(cmd==undefined){
                menu.toggleAttribute("hidden")
            }

            if(menu.hasAttribute('hidden')){
                document.querySelector('.menuButton').setIcon(0);
            }
        }
        
        window.menuClick=(evt, index)=>{
            window.pages[index].scrollIntoView();
            toogleMenu(null, false);
        }

        let checkPage=(e)=>{
            let h=e.srcElement.scrollHeight;
            let l=e.srcElement.scrollTop;
            let p=window.pages[0].clientHeight;
            let ht=h-p;

            let newSelected=Math.round(l/p);


            if(newSelected!=selected){

                if(newSelected==0){
                    document.querySelector(".down").removeAttribute("hidden");
                }else{
                    document.querySelector(".down").setAttribute("hidden", '');
                }

                window.pages[window.selected].deselect();
                window.selected=newSelected;
                window.pages[window.selected].select();
            }
        }
        
        data.pages.forEach((pageData, i) => {
            let page=new StPage(pageData);
            window.pages.push(page);

            if(pageData==data.pages[data.pages.length-1]){
                page.last=window.selected;
            }

            container.appendChild(page);

            let a=document.createElement("a");
            a.addEventListener("click", window.menuClick.bind(this, event, i));
            a.innerHTML=pageData.title;
            menu.appendChild(a);
        });

        container.addEventListener("scroll", checkPage);

        let resize = () => {
            let bottom=document.querySelector('footer');
            if(bottom){
                let box=bottom.getBoundingClientRect();
                document.body.style.setProperty('--total-height', box.bottom+ 'px');
            }
        }
        
        

        window.addEventListener("resize",resize);
        menuButton.addEventListener("click", toogleMenu.bind(this, event, undefined))

        window.pages[window.selected].select();
        resize();
    </script>
    
</head>
<body>
    <!-- novo site -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M748VBL"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="videoCrop">
        <div class="video-wrapper animated fadeIn index"><iframe id="bgvideo" src="https://player.vimeo.com/video/363859576?muted=1&amp;autoplay=1&amp;loop=1&amp;background=1" allow="autoplay; fullscreen" allowfullscreen="allowfullscreen" frameborder="0" class="background animated fadeIn"></iframe></div>
    </div>
    
    <div class="container"></div>

    <footer onclick="safeExit()">
        <div class="safeEx">SAFE EXIT</div>
        <st-svg class="exit" src="/assets/exit.svg"></st-svg>
        <st-svg class="down" src="/assets/down.svg"></st-svg>
    </footer>

    <nav hidden>
        <a href="tel:18007997233" data-v-4a866c26="" id="callforhelp">Call for Help ( +1 800 799 7233 )</a>
    </nav>

    <div class="pink more" hidden>
            <st-svg src="/assets/exit.svg" onclick="closePink()"></st-svg>

            <st-text>
                <span>
                    <img src="/assets/ncadv-logo.png" style="filter: saturate(0) brightness(2);">
                    Santander is partnering with the <a mark href="https://www.ncadv.org" target="_blank">NCADV</a> to offer financial education. Access to lessons on skills such as budgeting and finding housing empowers survivors to take back control of their lives and leave their abusers for good. The webinars cover a range of curriculum, from basic money management skills to how to build credit. If you or someone close to you is in need of these resources, you can find them on the previous page.
                </span>
            </st-text> 

            <st-text>
                <span>
                    <img src="/assets/nnedv-logo.png" style="filter: saturate(0) brightness(1.2); height:25pt;">
                    Another important step in reaching financial self-sufficiency is building credit. That’s why Santander is making a donation to the
                    <a href="https://www.nnedv.org" target="_blank">NNEDV</a> to help fund and develop their micro-lending program. This project helps survivors improve their credit scores by awarding them microloans of $100 that they pay back with no interest over the course of 10 months. Santander has committed to providing resources and support to evolve this program so that even more survivors can reach financial freedom.
                </span>
            </st-text> 
    </div>

    <div class="hotspot pink" hidden>
            <st-svg src="/assets/exit.svg" onclick="closeHotspot()"></st-svg>
            <st-text>
                <span>
                    In Someone Else’s Shoes is an initiative created to shed light on people who deserve more respect. When we have the chance to see how other people live, the world becomes a more empathetic, and therefore, respectful place for everyone. That’s why we’ve partnered with the National Coalition Against Domestic Violence <a href="https://www.ncadv.org" target="_blank">NCADV</a> and the National Network to End Domestic Violence <a href="https://www.nnedv.org" target="_blank">NNEDV</a>, to help those affected by domestic violence get the respect that they deserve.
                </span>
            </st-text> 
    </div>


    <header>
        <svg onclick="showHotspot()" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 245.28 42.92" style="enable-background:new 0 0 245.28 42.92;"  preserveAspectRatio="xMinYMid" id="logo">
            <style type="text/css">
                path{
                    fill:var(--color-red);
                }
            </style>
            <path  d="M245.28,19.48c0,1.44,0,2.88-0.37,3.97c-1.08-0.36-2.17-0.36-3.6-0.36c-1.08,0-2.17,0.36-3.25,0.36 c0,18.03,0,18.03,0,18.03c-4.69,0-4.69,0-4.69,0c0-20.92,0-20.92,0-20.92c1.8-0.72,5.4-1.44,7.94-1.44 C242.75,19.12,244.19,19.12,245.28,19.48z M213.89,28.49c10.82,0,10.82,0,10.82,0c0-3.25-2.17-5.41-5.42-5.41 C216.06,23.08,214.26,24.89,213.89,28.49z M229.04,32.46c-14.78,0-14.78,0-14.78,0c0.35,3.97,2.52,5.77,6.85,5.77 c2.52,0,5.04-0.72,7.57-1.8c-0.37,1.44-0.72,2.89-0.72,4.33c-2.17,1.08-4.7,1.44-6.85,1.44c-7.94,0-11.91-4.33-11.91-11.54 c0-6.49,3.24-11.54,10.46-11.54c6.85,0,9.74,4.33,9.74,9.74C229.4,30.3,229.04,31.38,229.04,32.46z M200.91,36.79 c0-13.35,0-13.35,0-13.35c-1.08,0-2.52-0.36-3.6-0.36c-4.69,0-6.85,2.88-6.85,7.93c0,4.33,1.8,7.21,5.77,7.21 C198.02,38.23,199.47,37.51,200.91,36.79z M205.59,11.9c0,29.58,0,29.58,0,29.58c-4.32,0-4.32,0-4.32,0 c-0.37-2.52-0.37-2.52-0.37-2.52c-1.08,1.8-2.89,3.25-6.12,3.25c-5.42,0-9.02-4.33-9.02-11.18c0-7.57,3.97-11.9,11.54-11.9 c1.45,0,2.52,0.36,3.6,0.36c0-6.85,0-6.85,0-6.85C202.35,11.9,204.16,11.9,205.59,11.9z M181.79,27.41c0,14.07,0,14.07,0,14.07 c-4.69,0-4.69,0-4.69,0c0-13.34,0-13.34,0-13.34c0-3.25-1.08-5.05-5.77-5.05c-1.08,0-2.17,0.36-3.62,0.36c0,18.03,0,18.03,0,18.03 c-4.69,0-4.69,0-4.69,0c0-20.92,0-20.92,0-20.92c2.89-0.72,6.14-1.44,8.66-1.44C179.27,19.12,181.79,22.36,181.79,27.41z M153.29,36.79c0-13.35,0-13.35,0-13.35c-1.08,0-2.17-0.36-3.24-0.36c-5.05,0-7.22,3.25-7.22,7.93c0,4.33,1.8,7.21,5.77,7.21 C150.41,38.23,152.21,37.87,153.29,36.79z M157.98,20.56c0,20.92,0,20.92,0,20.92c-4.32,0-4.32,0-4.32,0 c-0.37-2.52-0.37-2.52-0.37-2.52c-1.08,1.8-2.89,3.25-5.77,3.25c-5.77,0-9.37-4.33-9.37-11.18c0-7.57,3.97-11.9,11.91-11.9 C152.94,19.12,155.46,19.48,157.98,20.56z M132.73,38.23c1.45,0,2.89-0.36,3.97-0.72c-0.35,1.08-0.35,2.52-0.72,3.97 c-1.08,0.36-2.52,0.72-3.97,0.72c-4.33,0-7.22-2.16-7.22-7.21c0-22.36,0-22.36,0-22.36c1.45-0.36,2.89-0.72,4.33-0.72 c0,7.94,0,7.94,0,7.94c7.57,0,7.57,0,7.57,0c0,1.44-0.35,2.89-0.35,3.97c-7.22,0-7.22,0-7.22,0c0,10.46,0,10.46,0,10.46 C129.13,37.15,130.58,38.23,132.73,38.23z M120.11,27.41c0,14.07,0,14.07,0,14.07c-4.69,0-4.69,0-4.69,0c0-13.34,0-13.34,0-13.34 c0-3.25-1.08-5.05-5.77-5.05c-1.08,0-2.52,0.36-3.97,0.36c0,18.03,0,18.03,0,18.03c-4.69,0-4.69,0-4.69,0c0-20.92,0-20.92,0-20.92 c3.25-0.72,6.49-1.44,8.66-1.44C117.59,19.12,120.11,22.36,120.11,27.41z M91.26,36.79c0-13.35,0-13.35,0-13.35 c-0.72,0-1.8-0.36-3.25-0.36c-4.69,0-7.21,3.25-7.21,7.93c0,4.33,1.8,7.21,6.13,7.21C88.73,38.23,90.17,37.87,91.26,36.79z M95.95,20.56c0,20.92,0,20.92,0,20.92c-4.33,0-4.33,0-4.33,0c0-2.52,0-2.52,0-2.52c-1.44,1.8-3.25,3.25-6.13,3.25 c-5.41,0-9.38-4.33-9.38-11.18c0-7.57,3.97-11.9,11.9-11.9C91.26,19.12,93.78,19.48,95.95,20.56z M53.74,40.76 c0-1.8,0.36-2.89,0.72-4.33c2.52,1.08,5.41,1.44,7.93,1.44c3.97,0,6.13-1.08,6.13-3.61c0-2.52-1.8-3.61-5.77-5.41 c-2.16-0.72-2.16-0.72-2.16-0.72c-3.97-1.8-6.49-3.97-6.49-8.66c0-4.69,3.25-7.94,10.1-7.94c2.89,0,5.41,0.36,7.57,1.08 c0,1.8-0.36,3.25-0.72,4.33c-2.16-0.72-5.05-1.08-6.85-1.08c-3.61,0-5.41,1.44-5.41,3.61c0,2.16,1.44,3.61,4.69,4.69 c2.16,1.08,2.16,1.08,2.16,1.08c5.41,2.16,7.57,4.69,7.57,8.66c0,5.05-3.61,8.3-10.82,8.3C58.79,42.2,55.91,41.84,53.74,40.76z M32.46,19.84c-0.36-1.8-0.72-3.25-1.44-4.69C23.81,2.88,23.81,2.88,23.81,2.88c-0.36-0.72-0.72-1.8-1.08-2.88c0,0.72,0,0.72,0,0.72 c-1.8,2.89-1.8,6.49,0,9.74c5.41,9.74,5.41,9.74,5.41,9.74c1.8,2.89,1.8,6.85,0,9.74c-0.36,0.72-0.36,0.72-0.36,0.72 c0-1.08-0.36-2.16-1.08-3.25c-5.05-9.02-5.05-9.02-5.05-9.02c-3.25-5.41-3.25-5.41-3.25-5.41c-0.72-1.08-1.08-2.16-1.08-3.25 c-0.36,0.72-0.36,0.72-0.36,0.72c-1.8,2.89-1.8,6.49,0,9.74l0,0c5.77,9.74,5.77,9.74,5.77,9.74c1.44,3.25,1.44,6.85,0,9.74 c-0.36,0.72-0.36,0.72-0.36,0.72c-0.36-1.08-0.72-2.16-1.08-2.89c-7.21-12.26-7.21-12.26-7.21-12.26c-1.08-1.8-1.44-3.61-1.44-5.41 C5.05,21.64,0,25.97,0,30.66c0,6.85,10.1,12.26,22.72,12.26c12.26,0,22.36-5.41,22.36-12.26C45.09,25.97,40.04,21.64,32.46,19.84z"/>
        </svg>

        <div class="subLogo" onclick="showHotspot()">IN SOMEONE ELSE'S SHOES</div>

        <st-toggle-icon class="menuButton" icon1="/assets/burger.svg" icon2="/assets/exit.svg"></st-toggle-icon> 

        <st-toggle-icon icon1="/assets/audio_on.svg" icon2="/assets/audio_off.svg" enabled='1' onclick="toggleAudio()"></st-toggle-icon>
    </header>
    <div class="overlay" hidden>
        <st-svg src="/assets/exit.svg" onclick="closeVideo()"></st-svg>
        <iframe height= "100%" width="100%" id="overlayiframe" src="https://player.vimeo.com/video/359602920?autoplay=0&loop=0&autopause=1"   frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>

</body>

    <script>
        var iframe = document.querySelector('#bgvideo');
        var iframeOverlay = document.querySelector('#overlayiframe');
        var player = new Vimeo.Player(iframe);
        var overlayPlayer= new Vimeo.Player(iframeOverlay);

        var cueCoint=undefined;

        let firstPlay=()=>{
            player.off('play', firstPlay);
            document.querySelector(".videoCrop").style.opacity=0.7;
        }


        let touchPlay=()=>{
            player.play();
            document.body.removeEventListener("touchstart", touchPlay)
        }
        

        player.on('loaded', function(data) {
            document.body.addEventListener("touchstart", touchPlay);
        });;

        
        window.interval=undefined;
        let bufferstart=()=>{
            player.pause().then(()=>{
                window.clearInterval(window.interval);
                player.off('bufferstart', bufferstart);
                player.on('play', firstPlay);
                window.changeCue(window.pages[window.selected].data.cue);
            }).catch(function(e){
                window.interval=window.setInterval(() => {
                    bufferstart()
                }, 200);
            });
            
        }
        player.on('bufferstart', bufferstart);
        

        window.changeCue=function(data){
            player.setCurrentTime(data.start)
            player.play().then(()=>{}).catch((e)=>{});

            player.on("cuepoint", ()=>{
                player.setCurrentTime(data.start);
            })
            
            if(cueCoint){
                player.removeCuePoint(cueCoint).then(()=>{
                    
                }).catch((e)=>{
                });
            }

            player.addCuePoint(data.end, {
                // customKey: 'customValue'
            }).then(function(id) {
                cueCoint=id;
            })
        };

        window.safeExit=function(e){
            location.replace("/exit");
        }

        window.closeVideo=function(){
            overlayPlayer.pause().then(()=>{}).catch((e)=>{});
            player.play().then(()=>{}).catch((e)=>{});
            document.querySelector(".overlay").setAttribute("hidden", "");
        }

        window.playVideo=function(){
            window.closePink();
            window.closeHotspot();
            document.querySelector(".overlay").removeAttribute("hidden");
            player.pause().then(()=>{}).catch((e)=>{});
            overlayPlayer.setCurrentTime(0);
            overlayPlayer.setLoop(false);
            overlayPlayer.play();
        }


        window.closePink=function(){
            document.querySelector(".pink").setAttribute("hidden", "");
        }

        window.showPink=function(){
            document.querySelector(".pink").removeAttribute("hidden");
            window.closeHotspot();
        }

        window.closeHotspot=function(){
            document.querySelector(".hotspot").setAttribute("hidden", "");
        }

        window.showHotspot=function(){
            document.querySelector(".hotspot").removeAttribute("hidden");
            window.closePink();
        }


        ///////////// AUDIO

        window.currentAudio=undefined;
        window._muted=false;
        window.audioUnlocked=false;

        window.muted=function(valor){
            if(valor==undefined){
                return window._muted;
            }else{
                window._muted=valor;
                if(valor==true && audio.isPlaying){
                    window.audio.stop(); 
                }else if(valor==false && window.currentAudio!=undefined){
                    playAudio(currentAudio);
                }
                
            }
        }

        window.firstAudio=function(){
            window.audioUnlocked=true;
            let firstfile='fact1'
            document.body.removeEventListener("mousedown", firstAudio);
            document.body.removeEventListener("touchstart", firstAudio);
            window.audio.play( currentAudio || firstfile);

            if( muted() || currentAudio!=firstfile){
                if(audio.isPlaying){
                    window.audio.stop();    
                }
            }
        }

        window.audio = new VolumeSample(
            {
                fact1: '/assets/audiofact1.mp3',
                fact2: '/assets/audiofact2.mp3',
                fact3: '/assets/audiofact3.mp3',
                fact4: '/assets/audiofact4.mp3'
            },
            ()=>{
                document.body.addEventListener("mousedown", firstAudio);
                document.body.addEventListener("touchstart", firstAudio);
            }
        );

        window.playAudio=function(audioFile){
            if(audioUnlocked){
                if(audio.isPlaying){
                    window.audio.stop();
                }

                if(!muted() && audio.isPlaying==false){
                    window.audio.play(audioFile);
                }
            }
            window.currentAudio=audioFile;
        }

        window.leaveAudio=function(){
            window.currentAudio=undefined;
            if(audio.isPlaying){
                window.audio.stop();
            }
        }

        window.toggleAudio=function(){
            muted(!muted());
        }

        ///////////// TAG

        ga('create', 'GTM-M748VBL', 'auto')

    </script>
</html>