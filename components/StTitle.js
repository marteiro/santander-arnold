export class StTitle extends HTMLElement {

    static get observedAttributes() { return []; }

    constructor(data) {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = `
            <style>
                :host{
                    display:block;
                    box-sizing: border-box;
                    border-top: var(--line-width) solid var(--color-white);
                    padding-top: var(--spacing);
                    margin-bottom: var(--spacing);
                    position:relative;
                }

                ::slotted(span){
                    font-size:var(--text-size-title);
                    color:var(--color-white);
                    display:block;
                    line-height:1.1em;
                }

                :host .preTitle{
                    position:absolute;
                    top: var(--spacing-smaller);
                    font-size:var(--text-size-label);
                    color:var(--color-white);
                }
            </style>
            <span class="preTitle">${data.pretitle ? data.pretitle : ''}</span>
            <slot></slot>
        `;
        this._shadow = shadowRoot;
        let span=document.createElement("span");
        span.innerHTML=data.content;
        this.appendChild(span);
    }

    connectedCallback() {
    }
}

window.customElements.define('st-title', StTitle);
