<?php
class Meta
{
    public $title;
    public $description;
    public $img;

    public function __construct(Share $share, Router $router)
    {
        if (!empty($share->shares[$router->uri])) {
            $this->title = !empty($share->shares[$router->uri]['title']) ? $share->shares[$router->uri]['title'] : null;
            $this->description = !empty($share->shares[$router->uri]['description']) ? $share->shares[$router->uri]['description'] : null;
            $this->img = !empty($share->shares[$router->uri]['img']) ? $share->shares[$router->uri]['img'] : null;
        }
    }
}
